package com.formacionspring.web.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.formacionspring.web.step.Tarea;
import com.formacionspring.web.step.Tarea2;
import com.formacionspring.web.step.Tarea3;


//anotacion encargada de definir una clase como 
//configuracion para el framework
@Configuration
//proporciona una configuracion básica para crear trabajos por lotes.
//dentro de esta configuracion base se crea una instancia de StepScope
//ademas de una cantidad de beans disponibles para ser conectados automaticamente
@EnableBatchProcessing
public class ConfigBatch {
	
	//objeto constructor de steps
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	//objeto constructor de jobs
	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	
	//metodo step o definicion de procesos a ejecutar
	//lo agregamos al repositorio de spring con @Bean
	
	@Bean
	public Step step1() {
		return stepBuilderFactory.get("paso1").tasklet(new Tarea()).build();
	}
	
	@Bean
	public Step step2() {
		return stepBuilderFactory.get("paso2").tasklet(new Tarea2()).build();
	}
	
	@Bean
	public Step step3() {
		return stepBuilderFactory.get("paso3").tasklet(new Tarea3()).build();
	}
	//metodo para definir como se procesaran los steps
	//agregamos al bean para tenerlo en el repositorio de spring
	@Bean
	public Job HolaMundoJob() {
		return jobBuilderFactory.get("holamundoJob").start(step1()).next(step2()).next(step3()).build();
	}
	

}
