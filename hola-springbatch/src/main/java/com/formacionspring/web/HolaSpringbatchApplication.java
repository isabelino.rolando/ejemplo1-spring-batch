package com.formacionspring.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HolaSpringbatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(HolaSpringbatchApplication.class, args);
	}

}
