package com.formacionspring.web.step;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

//El tasklet es un objeto que contiene cualquier logica,
//que va a ser definida en un step y ejecutada en un job 
public class Tarea2 implements Tasklet{

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		// va a ir lo que ejecutemos
		System.out.print("Ejecutando Tarea 2");
		return RepeatStatus.FINISHED;
	}

}
